# Developed By Nalin Ahuja, nalin.ahuja@abzooba.com

import plotly.graph_objs as go
import numpy as np

import xpresso.ai.core.data.visualization.plotly.res.utils as utils
from xpresso.ai.core.data.visualization.plotly.res.plot_types import diagram
from xpresso.ai.core.commons.utils.constants import EMPTY_STRING


# End Imports-------------------------------------------------------------------------------------------------------------------------------------------------------------------

class new_plot(diagram):
    def __init__(self, input_1, input_2, plot_title=None, zero_filter=
    False, name=None, output_format=utils.HTML, output_path=None, auto_render=False, filename=None,
                 axes_labels=None):

        # Add Style to Plot
        self.style_plot()
        if axes_labels is None:
            axes_labels = dict()
            axes_labels["x_label"] = EMPTY_STRING
            axes_labels["y_label"] = EMPTY_STRING
        if plot_title is not None or filename is not None:
            self.set_labels_manual(plot_label=plot_title, filename=filename)
            self.set_labels(plot_label=plot_title, x_axis_label=axes_labels["x_label"],
                            y_axis_label=axes_labels["y_label"])

        # Convert Inputs to Lists
        input_1 = (np.array(input_1).tolist())
        input_2 = (np.array(input_2).tolist())
        # Check Input Types
        if (type(input_1) is list) and (type(input_2) is list):
            # Set List Values
            self.x_axis_values = input_1
            self.y_axis_values = input_2

            # Return Error on Differing List Lengths
            if (not ((len(self.x_axis_values) == len(self.y_axis_values)))):
                print("ERROR: Differing list lengths!")
                utils.halt()
            else:
                # Process Filter Argument
                self.filter_data(zero_filter, self.x_axis_values, self.y_axis_values)

                # Process Set Arguments
                if (not (name is None)):
                    self.set_name(name)
                if (not (output_path is None)):
                    self.output_path = output_path
                if (auto_render):
                    self.render(output_format=output_format, auto_open=True,
                                output_path=output_path)

        elif (type(input_1) is dict) and (type(input_2) is str):
            self.x_axis_values = []
            self.y_axis_values = []

            # Convert Dict to Lists
            for key in input_1:
                if (input_2 == "KEY_X"):
                    self.x_axis_values.append(key)
                    self.y_axis_values.append(input_1[key])
                elif (input_2 == "KEY_Y"):
                    self.x_axis_values.append(input_1[key])
                    self.y_axis_values.append(key)
                else:
                    # Return Error on Invalid Flag
                    print("ERROR: Invalid conversion flag!")
                    utils.halt()

            # Process Filter Argument
            self.filter_data(zero_filter, self.x_axis_values, self.y_axis_values)

            # Process Set Arguments
            if (not (name is None)):
                self.set_name(name)
            if (not (output_path is None)):
                self.output_path = output_path
            if (auto_render):
                self.render(output_format=output_format, auto_open=True,
                            output_path=output_path)
        else:
            # Return Error on Invalid Input
            print("ERROR: Invalid Input Type!")
            utils.halt()

    # End Object Constructor----------------------------------------------------------------------------------------------------------------------------------------------------

    def style_plot(self):
        # Apply Default Style to Plot
        self.apply_default_style()

        # Apply Custom Style to Plot
        self.marker = None

    # End Style Application Function--------------------------------------------------------------------------------------------------------------------------------------------

    def set_graph_color(self, color=None):
        if (not (color is None)):
            color_arr = []

            # Set Color List with Specified Color
            for i in range(0, len(self.x_axis_values)):
                color_arr.append(color)

            # Set Marker with color_arr
            self.marker = dict(color=color_arr)

    def show_extrema(self, extrema_type):
        # Check for Valid Input
        if (extrema_type == str("MIN") or extrema_type == str("MAX") or extrema_type == str("ALL")):
            color_arr = []

            # Color List
            std_highlight = 'rgba(204,204,204,1)'
            min_highlight = 'rgba(222,45,38,0.8)'
            max_highlight = 'rgba(38,222,44,0.8)'

            # Apply Neutral Color Style
            for i in range(0, len(self.x_axis_values)):
                color_arr.append(std_highlight)

            # Find Min/Max and Assign Colors
            if (extrema_type == str("min")):
                color_arr[self.y_axis_values.index(min(self.y_axis_values))] = min_highlight
            elif (extrema_type == str("max")):
                color_arr[self.y_axis_values.index(max(self.y_axis_values))] = max_highlight
            elif (extrema_type == str("both")):
                color_arr[self.y_axis_values.index(max(self.y_axis_values))] = max_highlight
                color_arr[self.y_axis_values.index(min(self.y_axis_values))] = min_highlight

            # Set Marker with color_arr
            self.marker = dict(color=color_arr)
        else:
            # Return Error on Invalid Flag
            print("ERROR: Invalid higlight flag!")
            utils.halt()

    # End Custom Style Functions------------------------------------------------------------------------------------------------------------------------------------------------

    def process(self, output_format):

        if output_format is utils.PNG or output_format is utils.PDF:
            # Generate Plot Layout and config for saved plots
            plot_data = go.Bar(x=self.x_axis_values, y=self.y_axis_values,
                               hovertemplate=self.hover_tool, marker=
                               self.marker, name=self.graph_name, text=
                               self.y_axis_values, textposition='auto')
        else:
            plot_data = go.Bar(x=self.x_axis_values, y=self.y_axis_values,
                               hovertemplate=self.hover_tool, marker=
                               self.marker, name=self.graph_name)

        plot_layout = go.Layout(autosize=True, width=self.plot_width,
                                height=self.plot_height, paper_bgcolor=str(self.bg_color),
                                plot_bgcolor=str(self.plot_color),
                                title=self.plot_title, xaxis=self.x_axis_title, yaxis=self.y_axis_title,
                                margin=go.layout.Margin(l=int(self.left), r=int(self.right), b=int(self.bottom),
                                                        t=int(self.top), pad=int(self.padding)))

        return {'data': [plot_data], 'layout': plot_layout}

    def render(self, output_format=utils.HTML, output_path=None, auto_open=True):
        self._render_single_plot(__name__, output_format=output_format, output_path=output_path, auto_open=auto_open)

    # End Generation/Rendering Functions----------------------------------------------------------------------------------------------------------------------------------------


# End Graph Class---------------------------------------------------------------------------------------------------------------------------------------------------------------

class join_plots(diagram):
    def __init__(self, plots, group_type, output_format=utils.HTML, output_path=None, auto_render=False):
        # Apply Def Style to Plot
        self.apply_default_style()

        # Check Input Types
        if (type(plots) is list and type(group_type) is str):
            self.multi_plots = []

            # group_type options ('stack', and 'group')
            if (group_type == "s"):
                self.bar_plot_type = "stack"
            elif (group_type == "g"):
                self.bar_plot_type = "group"
            else:
                # Return Error on Improper Flag
                print("ERROR: Improper Grouping Flag!")
                utils.halt()

            # Strip off styling from plots
            for plot in plots:
                self.multi_plots.append(plot.process(output_format)['data'][0])

            # Process Set Arguments
            if (not (output_path is None)):
                self.output_path = output_path
            if (auto_render):
                self.render(output_format=output_format, auto_open=True)
        else:
            # Return Error on Invalid Input
            print("ERROR: Invalid Input Type!")
            utils.halt()

    # End Object Constructor----------------------------------------------------------------------------------------------------------------------------------------------------

    def generate_layout(self):
        plot_layout = go.Layout(autosize=True, width=self.plot_width, height=self.plot_height,
                                paper_bgcolor=str(self.bg_color), plot_bgcolor=str(self.plot_color),
                                title=self.plot_title, xaxis=self.x_axis_title, yaxis=self.y_axis_title,
                                barmode=self.bar_plot_type,
                                margin=go.layout.Margin(l=int(self.left), r=int(self.right), b=int(self.bottom),
                                                        t=int(self.top), pad=int(self.padding)))

        return plot_layout

    def render(self, output_format=utils.HTML, output_path=None, auto_open=True):
        self._render_multi_plot(__name__, output_format=output_format, output_path=output_path, auto_open=auto_open)

    # End Generation/Rendering Functions----------------------------------------------------------------------------------------------------------------------------------------

# End Join Class----------------------------------------------------------------------------------------------------------------------------------------------------------------
