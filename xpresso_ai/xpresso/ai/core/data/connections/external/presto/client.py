"""Class design for Presto Connectivity"""

import json
import prestodb
import pandas as pd
import xpresso.ai.core.commons.utils.constants as constants
from xpresso.ai.core.data.connections.external.presto import presto_exception
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.logging.xpr_log import XprLogger


class PrestoConnector:
    """

    This class is used to interact with the any of the databases as per the configurations provided.
    The supported operation as of now are importing & exporting of data.

    :param presto_host_ip: Host IP for Presto Server (String object)
    :param presto_host_port: Host port for Presto Server (String object)
    :param presto_user: User name (String object)
    :param catalog: Identifying datasource (String object)
    :param schema: Identifying databases in the catalog (String object)

    """

    def __init__(self, user_input):
        """

        __init__() function initializes the host ip and port for Presto server.
        It also initiatiates the catalog and schema needed for Presto client.

        """

        self.logger = XprLogger()
        self.connector = None
        self.cursor = None
        with open(XprConfigParser().get_default_config_path()) as config_file:
            presto_config = json.load(config_file).get(constants.connectors)\
                .get(constants.presto)
        self.presto_host_ip = presto_config.get(constants.presto_ip)
        self.presto_host_port = presto_config.get(constants.presto_port)
        self.presto_user = presto_config.get(constants.presto_user)
        self.catalog = presto_config.get(constants.DSN).get(user_input).get(constants.catalog)
        self.schema = presto_config.get(constants.DSN).get(user_input).get(constants.schema)

    def get_connector(self):
        """

        This methods creates an connection to database with the provided configurations.

        :return None

        """

        try:

            if self.presto_host_ip is None:
                raise presto_exception.PrestoIPMissingInConfig
            if self.presto_host_port is None:
                raise presto_exception.PrestoPortMissingInConfig
            if self.catalog is None:
                raise presto_exception.PrestoCatalogMissingInConfig
            if self.schema is None:
                raise presto_exception.PrestoSchemaMissingInConfig

            connector = prestodb.dbapi.connect(host=self.presto_host_ip, port=self.presto_host_port,
                                               user=self.presto_user, catalog=self.catalog,
                                               schema=self.schema)
            return connector
        except presto_exception.PrestoIPMissingInConfig as exc:
            self.logger.exception("\nIP missing in Presto config file\n\n" + str(exc))
        except presto_exception.PrestoPortMissingInConfig as exc:
            self.logger.exception("\nPort missing in Presto config file\n\n" + str(exc))
        except presto_exception.PrestoCatalogMissingInConfig as exc:
            self.logger.exception("\nCatalog missing in Presto config file\n\n" + str(exc))
        except presto_exception.PrestoTableMissingInConfig as exc:
            self.logger.exception("\nTable missing in Presto config file\n\n" + str(exc))
        except Exception as exc:
            self.logger.exception("\nInvalid Attributes in the config. " + str(exc))
            raise presto_exception.PrestoConnectionException

    def import_data(self, table, columns):
        """

        This method makes an select query

        :param table: table name (String object)
        :param columns: names of columns as a comma-separated string
                        Put '*' in case of all columns (String object)

        :return a pandas DataFrame

        """

        try:
            self.connector = self.get_connector()
            query = "select %s from %s"
            self.cursor = self.connector.cursor()
            self.cursor.execute(query % (columns, table))
            results = self.cursor.fetchall()
            columns = []
            for column in self.cursor.description:
                columns.append(column[0])
            return pd.DataFrame(results, columns=columns)
        except Exception as exc:
            self.logger.exception(exc)
            raise presto_exception.PrestoImportException

    def close(self):
        """

        Method used to close all connections to the API

        :return: None

        """

        self.cursor.close()
        self.connector.close()
